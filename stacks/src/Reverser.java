public class Reverser {

    private String input;
    private String output;

    public Reverser(String in){
        input = in;
    }

    public String doReverse(){
        int stackSize = input.length();
        StackX myStack = new StackX(stackSize);

        for (int i = 0; i < stackSize; i++) {
            char ch = input.charAt(i);
            myStack.push(ch);
        }
        output = "";
        while (!myStack.isEmpty()){
            char ch = myStack.pop();
            output += ch;
        }
        return output;
    }




}
