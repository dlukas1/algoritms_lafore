public class StackX {

    private int maxSize; // Размер массива
    private char[] stackArray;
    private int top; // Вершина стека

    public StackX(int s){
        maxSize = s;
        stackArray = new char[maxSize];
        top = -1; // Пока нет ни одного элемента
    }

    public void push(char j){
        stackArray[++top] = j; // Увеличение top, вставка элемента
    }

    public char pop(){
        return stackArray[top--]; // Извлечение элемента, уменьшение top
    }

    public long peek(){
        return stackArray[top];
    }

    public boolean isEmpty(){
        return (top == -1);
    }

    public boolean isFull(){
        return (top == maxSize-1);
    }
}
