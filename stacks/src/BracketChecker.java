public class BracketChecker {

    private String input;

    public BracketChecker(String in){
        input = in;
    }

    public void check(){
        int stackSize = input.length();
        StackX myStack = new StackX(stackSize);

        for (int i = 0; i < stackSize; i++) {
            char ch = input.charAt(i);

            switch (ch){
                case '(':
                case '[':
                case '{':
                    myStack.push(ch);
                    break;

                case '}':
                case ']':
                case ')':
                    if (!myStack.isEmpty()){
                        char chx = myStack.pop(); // Извлечь и проверить
                        if (ch == '}' && chx != '{'||
                                ch == ']' && chx != '['||
                                ch == ')' && chx != '(')
                            System.out.println("Error " + ch + " at " + i);
                    } else // Преждевременная нехватка элементов
                        System.out.println("Error " + ch + " at " + i);
                    break;
                    default:
                        break;
            }
        }
        // В этой точке обработаны все символы
        if (!myStack.isEmpty())
            System.out.println("Error : missing right delimeter");
    }



}
