public class ArithmProgression extends Progression {

    protected long increment;

    public ArithmProgression(long stepsize, long start){
        super(start);
        increment = stepsize;
    }
    public ArithmProgression(){ //product 0,1,2,3...
        this(1,0);
    }
    public ArithmProgression(long stepsize){//0, stepsize, 2*stepsize
        this(stepsize, 0);
    }
    protected void advance(){
        current += increment;
    }

}
