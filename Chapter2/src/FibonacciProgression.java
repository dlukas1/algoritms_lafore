public class FibonacciProgression extends Progression {
    protected long prev;

    //traditional Fibonacci 0,1,1,2,3...
    public FibonacciProgression(){this(0,1);}

    public FibonacciProgression(long first, long second){
        super(first);
        prev = second-first;
    }

    protected void advance(){
        long temp = prev;
        prev = current;
        current += temp;
    }
}
