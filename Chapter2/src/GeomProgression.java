public class GeomProgression extends Progression {

    protected long base;

    //constructs 1,2,4,8,...
    public GeomProgression(){this(2,1);}

    //1,b,b^2, b^3...
    public GeomProgression(long b){this(b, 1);}


    public GeomProgression(long b, long start){
        super(start);
        base = b;
    }

    protected void advance(){
        current *= base;
    }

}
