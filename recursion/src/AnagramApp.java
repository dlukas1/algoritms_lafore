import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class AnagramApp {

    static int size;
    static int count;
    static char[] arrChar = new char[100];

    public static void main(String[] args) throws IOException {
        System.out.println("Enter a word: ");
        String input = getString();
        size = input.length();
        count=0;
        for (int i = 0; i < size; i++) {
            arrChar[i] = input.charAt(i);
        }
        doAnagram(size);
    }


    public static void doAnagram(int newSize){
        if (newSize == 1)
            return;
        for (int i = 0; i < newSize; i++) {
            doAnagram(newSize-1);
            if (newSize ==2)
                displayWord();
            rotate(newSize);
        }
    }


    // rotate left all chars from position to end
    public static void rotate(int newSize){
        int i;
        int position = size-newSize;
        char temp = arrChar[position];
        for (i = position+1; i < size; i++) {
            arrChar[i-1] = arrChar[i];
        }
        arrChar[i-1] = temp;
    }

    public static void displayWord(){

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {

            sb.append(arrChar[i]);
        }
        System.out.println(++count+" " + sb.toString());
        System.out.flush();
    }

    public  static String getString() throws IOException{
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String s = br.readLine();
        return s;
    }

}
