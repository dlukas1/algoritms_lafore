public class Queue {

    private int maxSize;
    private char[] queArray;
    private int front;
    private int rear;
    private int nItems;

    public Queue(int s){
        maxSize = s;
        queArray = new char[maxSize];
        front = 0;
        rear = -1;
        nItems = 0;
    }

    // Вставка элемента в конец очереди
    public void insert(char c){
        if (rear==maxSize-1) // Циклический перенос
            rear = -1;
        queArray[++rear] = c; // Увеличение rear и вставка
        nItems++;
    }

    // Извлечение элемента в начале очереди
    public char remove(){
        char temp = queArray[front++];
        if (front == maxSize)
            front = 0;
        nItems--;
        return temp;
    }

    // Чтение элемента в начале очереди
    public char peekFront(){
        return queArray[front];
    }

    public boolean isEmpty(){
        return (nItems == 0);
    }

    public boolean isFull(){
        return (nItems == maxSize);
    }

    public int size(){
        return nItems;
    }

}
