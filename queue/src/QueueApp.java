public class QueueApp {

    public static void main(String[] args) {
        Queue queue = new Queue(5);
        queue.insert('1');
        queue.insert('2');
        queue.insert('3');
        queue.insert('4');

        queue.remove();
        queue.remove();
        queue.remove();

        queue.insert('5');
        queue.insert('6');
        queue.insert('7');
        queue.insert('8');

        while (!queue.isEmpty()){
            char c = queue.remove();
            System.out.println(c);
        }
    }


}
