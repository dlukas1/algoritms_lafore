public class InsertionSortedListApp {

    public static void main(String[] args) {
        int size = 10;
        smallLink[]linkArr = new smallLink[size];
        for (int i = 0; i < size; i++) {
            int n = (int)(java.lang.Math.random()*99);
            smallLink newLink = new smallLink(n);
            linkArr[i] = newLink;
        }
        System.out.println("Unsorted array: ");
        for (int i = 0; i < size; i++) {
            System.out.println(linkArr[i].dData + " ");
        }

        InsertionSortedList insertionSortedList = new InsertionSortedList(linkArr);
        for (int i = 0; i < size; i++) {
            linkArr[i] = insertionSortedList.remove();
        }
        System.out.println("Sorted array: ");
        for (int i = 0; i < size; i++) {
            System.out.println(linkArr[i].dData + " ");
        }
    }

}
