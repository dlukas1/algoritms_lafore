public class LinkedQueApp {

    public static void main(String[] args) {
        LinkedQue theQue = new LinkedQue();
        theQue.insert(1,1);
        theQue.insert(2,2);

        theQue.displayQue();

        theQue.insert(3,3);
        theQue.insert(4,4);
        theQue.displayQue();

        theQue.remove();
        theQue.remove();
        theQue.displayQue();
    }

}
