public class LinkedQue {
    private CycleList theList;

    public LinkedQue(){
        theList = new CycleList();
    }

    public boolean isEmpty(){
        return theList.isEmpty();
    }
    public void insert(int id, long dd){
        theList.insertLast(id,dd);
    }
    public Link remove(){
        return theList.deleteFirst();
    }
    public void displayQue(){
        System.out.println("Que (front --> end)");
        theList.dislpayList();
    }
}
