public class Link {

    public int iData;
    public double dData;
    public Link next;

    public Link(int id, double dd){
        iData = id;
        dData = dd;
        //next is null by default
    }

    public void displayLink(){
        System.out.println("{" + iData + ", " + dData + "}");
    }

}
