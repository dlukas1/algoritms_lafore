public class CycleList {

    private Link first;
    private Link last;

    public CycleList(){
        first = null;
        last = null;
    }

    public boolean isEmpty(){
        return (first == null);
    }

    public void insertFirst(int id, double dd){
        Link newLink = new Link(id, dd);
        if (isEmpty()){
            last = newLink;
        }
        newLink.next = first;
        first = newLink;
    }

    public void insertLast(int id, long dd){
        Link newLink = new Link(id, dd);
        if (isEmpty())
            first = newLink;
        else {
            last.next = newLink;
        }
        last = newLink;
    }

    public Link deleteFirst(){
        Link temp = first;
        if (first.next==null)
            last = null;
        System.out.println("Deleted: ");
        first.displayLink();
        first = first.next;
        return temp;
    }
    public Link deleteLast(){
        Link temp = last;
        System.out.println("Deleted " );
        last.displayLink();
        last = null;
        return temp;
    }

    public void dislpayList(){
        System.out.println("List (first --> last):");
        Link current = first;
        while (current != null){
            current.displayLink();
            current = current.next;
        }
    }



}
