public class InsertionSortedList {
    private smallLink first;

    public InsertionSortedList(){first = null;}

    public InsertionSortedList(smallLink[]linkArr){// Конструктор (аргумент - массив)
        first = null;
        for (int i = 0; i < linkArr.length; i++) {
            insert(linkArr[i]);
        }
    }

    public void insert(smallLink k){
        smallLink prev = null;
        smallLink curr = first;

        while (curr != null && k.dData > curr.dData){
            prev = curr;
            curr = curr.next;
        }
        if (prev == null){
            first = k;
        }
        else {
            prev.next = k;
        }
        k.next = curr;
    }

    public smallLink remove(){
        smallLink temp = first;
        System.out.println("Removed:");
        first.displayLink();
        first = first.next;
        return temp;
    }
}
