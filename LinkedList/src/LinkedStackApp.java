public class LinkedStackApp {
    public static void main(String[] args) {
        LinkedStack theStack = new LinkedStack();
        theStack.push(1,1);
        theStack.push(2,2);

        theStack.displayStack();

        theStack.push(3,3);
        theStack.push(4,4);

        theStack.displayStack();
        theStack.pop();
        theStack.pop();

        theStack.displayStack();
    }
}
