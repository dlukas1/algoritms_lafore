public class LinkListApp {
    public static void main(String[] args) {
        LinkList theList = new LinkList();
        theList.insertFirst(11,1.09);
        theList.insertFirst(22,2.09);
        theList.insertFirst(33,3.09);
        theList.insertFirst(44,4.09);

        theList.displayList();

        Link f = theList.find(33);
        System.out.println("Found link with key-value :" + f.iData + " - "+ f.dData);


        theList.delete(33);
        theList.displayList();

        /*
        while (!theList.isEmpty()){
            Link aLink = theList.deleteFirst();
            System.out.println("Deleted ");
            aLink.displayLink();
        }*/
    }
}
