public class SortedList {

    private smallLink first;

    public SortedList(){
        first = null;
    }

    public boolean isEmpty(){
        return (first == null);
    }

    public void insert(long key){
        smallLink newLink = new smallLink(key);
        smallLink previous = null;
        smallLink current = first;

        while (current != null && key>current.dData){
            previous = current;
            current = current.next;
        }
        if (previous == null){
            first = newLink;
        } else {
            previous.next = newLink;
        }
        newLink.next = current;
    }

    public smallLink remove(){
        smallLink temp = first;
        System.out.println("Remove: ");
        first.displayLink();
        first = first.next;
        return temp;
    }

    public void dislpayList(){
        System.out.println("List (first --> last): ");
        smallLink current = first;
        while (current != null){
            current.displayLink();
            current = current.next;
        }
    }
}
