package com.company;

import java.lang.reflect.Array;

public class Sortings {
    private long [] a;
    private int nElems;


    public Sortings(int max){
        a = new long[max];
        nElems = 0;
    }

    public void insert(long val){
        a[nElems] = val;
        nElems++;
    }

    public void display(){
        for (int i = 0; i < nElems; i++) {
            System.out.println(a[i] + " ");
        }
        System.out.println("");
    }

    public void insertionSort(){
        int in, out;

        for ( out = 1; out < nElems ; out++) {
            long temp = a[out];
            in = out;
            while (in>0 && a[in-1] >= temp){
                a[in] = a[in-1];
                --in;
            }
            a[in] = temp;
        }
    }

    public  void bubbleSortArr(){
        int out, in;
        for ( out = nElems-1;  out>1 ; out--) {
            for ( in = 0; in < out; in++) {
                if (a[in] > a[in+1])
                    swap(in, in+1);
            }
        }
    }

    public void oddEvenSort(){
        for (int i = 0; i < nElems; i++) {
            for (int j = (i%2==1)?0:1; j < nElems-1; j++) {
                if (a[j]>a[j+1])
                {
                    long temp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = temp;
                }
            }
        }
    }

    public long median(){
        long mediana;
        bubbleSortArr();
        if (nElems%2==1) mediana = a[(nElems+1)/2];
        else {
             mediana = ( a[nElems/2] + a[(nElems/2)+1])/2;
        }
        return mediana;
    }

    public void selectionSort(){
        int out, in, min;

        for ( out = 0; out < nElems-1 ; out++) {
            min = out;
            for ( in = out+1; in < nElems; in++) {
                if (a[in] < a[min]) {
                    min = in;
                }
                    swap(out, min);

            }
        }
    }

    private void swap(int one, int two){
        long temp = a[one];
        a[one] = a[two];
        a[two] = temp;
    }


}
