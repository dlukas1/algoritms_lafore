package com.company;

public class Person {
    private String lastName;
    private String firstName;
    private int age;

    public Person(String last, String first, int a){
        this.lastName = last;
        this.firstName = first;
        this.age = a;
    }

    public void displayPerson(){
        System.out.println("Name: " + firstName + " " + lastName + ", age "+ age);
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge(){return age;}
}
