package com.company;

public class OrderedArrayApp {
    public static void main(String[] args) {
        OrderedArray odds = new OrderedArray(10);
        OrderedArray evens = new OrderedArray(10);

        //filling

        int oSeed = 1;
        int eSeed = 2;
        for (int i = 0; i < 10; i++) {
            odds.insert(oSeed);
            oSeed +=2;
        }
        for (int i = 0; i < 10; i++) {
            evens.insert(eSeed);
            eSeed +=2;
        }

        odds.display();
        evens.display();

        OrderedArray merged = new OrderedArray(20);
        merged=merged.merge(odds,evens);
        merged.display();
    }
}
