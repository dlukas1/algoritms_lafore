package com.company;

public class OrderedArray {
    private int []a;
    private int nElems;

    public OrderedArray(int max){
        a = new int[max];
        nElems = 0;
    }

    public OrderedArray merge(OrderedArray ar,OrderedArray b){
        OrderedArray merged = new OrderedArray(ar.size()+b.size());
        int biggestSize = (ar.size()>b.size())? ar.size():b.size();
        for (int i = 0; i < biggestSize; i++) {
                merged.insert((ar.a[i]>b.a[i])?b.a[i] : ar.a[i]);
        }
        return merged;
    }

    public void display(){
        System.out.println("Printing: ");
        for (int i = 0; i < nElems; i++) {
            System.out.println(a[i] + " ");

        }
        System.out.println("");
    }

    public int size(){
        return nElems;
    }

    public int find(int searchKey){
        int lowerBound = 0;
        int upperBound = nElems-1;
        int curIn;

        while (true){
            curIn = (lowerBound+upperBound)/2;
            if (a[curIn] == searchKey)
                return curIn;
            else if(lowerBound>upperBound){
                return nElems;
            }
            else {
                if (a[curIn]<searchKey){
                    lowerBound = curIn+1;
                } else {
                    upperBound = curIn-1;
                }
            }
        }
    }

    public void insert(int value) // Вставка элемента в массив
    {
        int j;
        for(j=0; j<nElems; j++) // Определение позиции вставки
            if(a[j] > value) // (линейный поиск)
                break;
        for(int k=nElems; k>j; k--) // Перемещение последующих элементов
            a[k] = a[k-1];
        a[j] = value; // Вставка
        nElems++; // Увеличение размера
    }
    }

