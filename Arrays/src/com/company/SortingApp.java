package com.company;

import java.util.Random;

public class SortingApp {
    public static void main(String[] args) {
        int maxSize = 1000000;
        long stime, ftime, diff;
        Sortings arr;
        arr = new Sortings(maxSize);

        Random r = new Random();
        for (int i = 0; i < 10; i++) {
            arr.insert(r.nextInt(10));
           // arr.insert(i);
        }
        arr.display();
        stime = System.nanoTime();
        //arr.bubbleSortArr();
        //arr.selectionSort();
        //arr.insertionSort();
        arr.oddEvenSort();
        ftime = System.nanoTime();
        diff = (ftime - stime)/1000;
        arr.display();
        System.out.println("Time passed " + diff);
        //arr.display();
       // System.out.println("Median is " + arr.median());
    }
}
