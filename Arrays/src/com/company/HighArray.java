package com.company;

public class HighArray {
    private long []a;
    private  int nElems;

    public HighArray(int max){
        a= new long[max];
        nElems = 0;
    }

    public void noDups(){
        // i is index for searchable element
        //j is index to go throught array
        for (int i = 0; i < nElems; i++) {
            for (int j = 0; j < nElems; j++) {
                if (i==j) continue;
                if (a[i] == a[j]){
                    System.out.println(a[i] + " dublicate of " + a[j]);
                    delete(a[j]);
                }
            }
        }
    }

    public void removeMax(){
        long max = getMax();
        delete(max);
    }

    public long getMax(){
        long max = 0;
        if(a.length==0)
            return -1;
        for (int i = 0; i < nElems; i++) {
            if (a[i]>max){
                max = a[i];
            }
        }
        return max;
    }

    public boolean find(long searchKey){
        boolean found = false;
        for (int i = 0; i < nElems; i++) {
            if (a[i] == searchKey)
                break;
            if (i == nElems)
                found = false;
            else found = true;
        }
        return found;
    }


    public void insert(long val){
        a[nElems] = val;
        nElems++;
    }
    
    public boolean delete(long val){
        boolean found = false;
        int i;
        for (i = 0; i < nElems; i++) {
            if (a[i] == val){
                 found = true;
                 nElems--;
                 break;
            }

        }
        for (int j = i; j <nElems ; j++) {
            a[j] = a[j+1];
        }
        return found;
    }

    public void display(){
        for (int i = 0; i < nElems; i++) {
            System.out.println(a[i] + " ");

        }
        System.out.println("");
    }
}
