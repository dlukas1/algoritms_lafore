package com.company;

public class ClassDataArray {
    private Person[]a;
    private int nElems;

    public ClassDataArray(int max){
        a = new Person[max];
        nElems = 0;
    }

    public Person find(String searchName){
        int i;
        for (i = 0; i < nElems; i++)
            if (a[i].getLastName().equals(searchName)){
            break;
        }
            if (i ==nElems)
                return null;
            else return a[i];
    }

    public void insert(String last, String first, int ag){
        a[nElems] = new Person(last,first,ag);
        nElems++;
    }

    public int getnElems(){
        return nElems;
    }

    public boolean delete(String name){
        for (int i = 0; i < nElems; i++) {
            if(a[i].getLastName().equals(name))
                a[i] = a[i+1];
            nElems--;
            return true;
        }
        return false;
    }

    public void displayAll(){
        for (int i = 0; i < nElems; i++) {
            a[i].displayPerson();
        }
    }
}
