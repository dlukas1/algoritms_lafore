package com.company;

public class ClassDataApp {
    public static void main(String[] args) {
        int size = 100;
        ClassDataArray arr= new ClassDataArray(size);

        arr.insert("Clinton", "Bill", 54);
        arr.insert("Putin", "Vova", 62);
        arr.insert("Billy", "Boy", 24);
        arr.insert("McGregor", "Connar", 1024);
        arr.insert("Tyson", "Mike", 38);
        arr.insert("Trump", "Donald", 54);
        arr.insert("Obama", "Barak", 63);
        arr.insert("Hussain", "Saddam", 84);
        arr.insert("Tesla", "Nikola", 88);
        arr.insert("Lukas", "Dmitri", 33);


        arr.displayAll();

        String searcLName = "Trump";
        Person p = arr.find(searcLName);
        if (p != null){
            System.out.println("Found ");
            p.displayPerson();
        } else {
            System.out.println("Can't find " + searcLName);
        }

        arr.delete("Lukas");
        arr.displayAll();


    }
}
